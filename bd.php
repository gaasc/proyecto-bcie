﻿<?php
class bd
{
	public $con;
	public function __construct()
	{
	   $this->con = null;
	}
	function conk()
	{
		$this->con = null;
	}
	function constart()
	{
		try 
		{
			$this->con = new PDO("mysql:host=127.0.0.1;dbname=usr", 'root', '');
			$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		}
		catch(PDOException $e) 
		{
			echo $e->getMessage();
		}
	}
	function uex()
	{
		$usuario=$_POST["usr"];
		$query=$this->con->prepare("SELECT IF(EXISTS(SELECT * FROM `usr` WHERE usr = ?),1,0) AS result");
		$query->execute(array( $usuario));
		$result = $query->fetch();
		$count = $result[0];
		return $count;
	}
	function ine()
	{
		$usuario=$_POST["usr"];
		$pwd=$_POST["pwd"];
		$permit=$_POST["rol"];
		$query = $this->con->prepare("INSERT INTO `usr`(`usr`, `pwd`, `permit`) VALUES (?,?,?)");
		$query->execute(array($usuario,$pwd,$permit));

	}
	function validar()
	{
		if(isset($_POST) && $_POST)
		{
			$usuario=$_POST['usr'];
			$pass=hash("sha512", $_POST['pwd']);
			$query = $this->con->prepare("SELECT IF(EXISTS(SELECT * FROM `usr` WHERE usr = ? AND pwd = ?),1,0) AS result");
			$query->execute(array($usuario, $pass));
			$result = $query->fetch();
			$count = $result[0];
			if($count==0)
			{
				echo "<script type='text/javascript'>alert('Nombre de usuario o contraseña incorrectos, por favor intente de nuevo');</script>";	
			}
			else
			{
				$query=$this->con->prepare("SELECT `usr`, `permit` FROM `usr` WHERE `usr`= ?");
				$query->execute(array($usuario));
				$result = $query->fetch();
				$count = $result[1];
				$_SESSION["per"]=$count;
				$_SESSION["usr"]=$result[0];
				echo "<script type='text/javascript'>window.location.href = \"user.php\";</script>";
			}
		}	
	}
	function inno($from)
	{
		$this->constart();
		$table=$from["var2"];
		$year=$from["var3"];
		$query = $this->con->prepare("SELECT `key` , sum(`cant`) FROM `$table` where `ye`='".$year."' group by `key`");
		$query->execute();
		$result=$query->fetchall();
		return $result;
	}
	function llenar($type)
	{
		$this->constart();
		$permits=$_SESSION["per"];
		switch ($type) 
		{
			case 0:
				$query= $this->con->prepare("select apn from apermits as a inner join permits as b on a.apermits=b.apermits where permit=?");
				$query->execute(array($permits));
				$result=$query->fetchall();
				for($i=0;$i<count($result);$i++)
				{
					echo"<li><a href=\"\" data-transition=\"slide\">".$result[$i][0]."</a></li>";
				}
				break;
			case 1:
				$query= $this->con->prepare("select pnom, permit from permits group by Pnom");
				$query->execute();
				$result=$query->fetchall();
				for($i=0;$i<count($result);$i++)
				{
					echo "<option value=\"".(($result[$i][1]))."\">".$result[$i][0]."</option>";
				}
				break;
			default:
				# code...
				break;
		}
		
	
	}
	function edit($type)
	{
		$this->constart();
		$material=$_POST["usr"];
		$query = $this->con->prepare("SELECT IF(EXISTS(SELECT * FROM `usr` WHERE usr = ?),1,0) AS result");
			$query->execute(array($material));
			$result = $query->fetch();
			$count = $result[0];
			if (!$_SESSION)
			{
				echo "<script type='text/javascript'>alert('Usted no dispone de permisos suficientes para realizar esta operacion, contacte a su administrador');</script>";
			}
			else
			{
			if($count==0)
			{
				echo "<script type='text/javascript'>alert('El usuario que escribio no existe');</script>";	
			}
			else
			{
				switch($type)
				{	
					case 0:
						if($_SESSION["per"]!=1)
						{
							echo "<script type='text/javascript'>alert('Usted no dispone de permisos suficientes para realizar esta operacion, contacte a su administrador');</script>";
							echo $_SESSION["per"];
						}
						else
						{
							$permit=$_POST["rol"];
							$query= $this->con->prepare("UPDATE `usr`.`usr` SET `permit` = ? WHERE `usr`.`usr` = ?;");
							$query->execute(array($permit,$material));
						}
					break;
					case 1:
						$permit=$_POST["pwd"];
						$query= $this->con->prepare("UPDATE `usr`.`usr` SET `pwd` = ? WHERE `usr`.`usr` = ?;");
						$query->execute(array($permit,$material));
					break;
					default:
					break;		
				}	
			}	
		}
	}

}

?>

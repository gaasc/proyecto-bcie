-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2014 at 10:29 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `usr`
--
CREATE DATABASE IF NOT EXISTS `usr` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `usr`;

-- --------------------------------------------------------

--
-- Table structure for table `apermits`
--
-- Creation: Apr 03, 2014 at 09:13 PM
--

DROP TABLE IF EXISTS `apermits`;
CREATE TABLE IF NOT EXISTS `apermits` (
  `apermits` int(11) NOT NULL,
  `apn` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`apermits`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `apermits`
--

INSERT INTO `apermits` (`apermits`, `apn`) VALUES
(1, 'Auditoria Interna'),
(2, 'Gerencia Comercial');

-- --------------------------------------------------------

--
-- Table structure for table `aprv`
--
-- Creation: Apr 04, 2014 at 09:17 PM
--

DROP TABLE IF EXISTS `aprv`;
CREATE TABLE IF NOT EXISTS `aprv` (
  `ye` year(4) NOT NULL,
  `mes` int(11) NOT NULL,
  `key` varchar(25) COLLATE utf8_bin NOT NULL,
  `cant` decimal(11,2) NOT NULL,
  PRIMARY KEY (`ye`,`mes`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `aprv`
--

INSERT INTO `aprv` (`ye`, `mes`, `key`, `cant`) VALUES
(2010, 12, 'Belice', '10.70'),
(2010, 12, 'Colombia', '35.30'),
(2010, 12, 'Costa Rica', '218.50'),
(2010, 12, 'El Salvador', '132.90'),
(2010, 12, 'Guatemala', '332.90'),
(2010, 12, 'Honduras', '474.40'),
(2010, 12, 'Nicaragua', '135.80'),
(2010, 12, 'Panama', '12.80'),
(2010, 12, 'Rep.Dominicana', '150.00'),
(2011, 10, 'Argentina', '0.00'),
(2011, 10, 'Colombia', '50.00'),
(2011, 10, 'Costa Rica', '530.00'),
(2011, 10, 'El Salvador', '285.80'),
(2011, 10, 'Guatemala', '184.10'),
(2011, 10, 'Honduras', '392.20'),
(2011, 10, 'Nicaragua', '162.40'),
(2011, 10, 'Panama', '25.00'),
(2012, 5, 'Colombia', '0.00'),
(2012, 5, 'Costa Rica', '38.00'),
(2012, 5, 'El Salvador', '15.00'),
(2012, 5, 'Guatemala', '17.00'),
(2012, 5, 'Honduras', '271.10'),
(2012, 5, 'Nicaragua', '83.60'),
(2012, 6, 'Colombia', '0.00'),
(2012, 6, 'Costa Rica', '128.00'),
(2012, 6, 'El Salvador', '46.00'),
(2012, 6, 'Guatemala', '17.00'),
(2012, 6, 'Honduras', '295.50'),
(2012, 6, 'Nicaragua', '83.60'),
(2012, 6, 'Panama', '0.00'),
(2012, 7, 'Colombia', '0.00'),
(2012, 7, 'Costa Rica', '128.00'),
(2012, 7, 'El Salvador', '46.00'),
(2012, 7, 'Guatemala', '17.00'),
(2012, 7, 'Honduras', '328.70'),
(2012, 7, 'Nicaragua', '83.60'),
(2012, 7, 'Panama', '0.00'),
(2012, 8, 'Colombia', '0.00'),
(2012, 8, 'Costa Rica', '128.00'),
(2012, 8, 'El Salvador', '46.00'),
(2012, 8, 'Guatemala', '17.00'),
(2012, 8, 'Honduras', '380.70'),
(2012, 8, 'Nicaragua', '83.60'),
(2012, 8, 'Panama', '0.00'),
(2012, 9, 'Colombia', '0.00'),
(2012, 9, 'Costa Rica', '128.00'),
(2012, 9, 'El Salvador', '119.00'),
(2012, 9, 'Guatemala', '19.00'),
(2012, 9, 'Honduras', '380.70'),
(2012, 9, 'Nicaragua', '150.20'),
(2012, 9, 'Panama', '0.00'),
(2012, 10, 'Colombia', '0.00'),
(2012, 10, 'Costa Rica', '128.00'),
(2012, 10, 'El Salvador', '211.20'),
(2012, 10, 'Guatemala', '25.50'),
(2012, 10, 'Honduras', '380.70'),
(2012, 10, 'Nicaragua', '150.20'),
(2012, 10, 'Panama', '0.00'),
(2012, 11, 'Colombia', '0.00'),
(2012, 11, 'Costa Rica', '353.00'),
(2012, 11, 'El Salvador', '211.20'),
(2012, 11, 'Guatemala', '297.50'),
(2012, 11, 'Honduras', '380.70'),
(2012, 11, 'Nicaragua', '253.00'),
(2012, 11, 'Panama', '0.00'),
(2012, 12, 'Colombia', '0.00'),
(2012, 12, 'Costa Rica', '353.00'),
(2012, 12, 'El Salvador', '211.20'),
(2012, 12, 'Guatemala', '348.30'),
(2012, 12, 'Honduras', '354.60'),
(2012, 12, 'Nicaragua', '253.00'),
(2012, 12, 'Panama', '0.00'),
(2013, 1, 'Belice', '0.00'),
(2013, 1, 'Colombia', '0.00'),
(2013, 1, 'Costa Rica', '0.00'),
(2013, 1, 'El Salvador', '0.00'),
(2013, 1, 'Guatemala', '0.00'),
(2013, 1, 'Honduras', '0.00'),
(2013, 1, 'Nicaragua', '0.00'),
(2013, 1, 'Panama', '0.00'),
(2013, 1, 'Rep.Dominicana', '0.00'),
(2013, 2, 'Belice', '0.00'),
(2013, 2, 'Colombia', '0.00'),
(2013, 2, 'Costa Rica', '0.00'),
(2013, 2, 'El Salvador', '0.00'),
(2013, 2, 'Guatemala', '0.00'),
(2013, 2, 'Honduras', '0.00'),
(2013, 2, 'Nicaragua', '0.00'),
(2013, 2, 'Panama', '0.00'),
(2013, 2, 'Rep.Dominicana', '80.20'),
(2013, 3, 'Belice', '0.00'),
(2013, 3, 'Colombia', '0.00'),
(2013, 3, 'Costa Rica', '17.80'),
(2013, 3, 'El Salvador', '7.50'),
(2013, 3, 'Guatemala', '0.00'),
(2013, 3, 'Honduras', '56.80'),
(2013, 3, 'Nicaragua', '0.00'),
(2013, 3, 'Panama', '0.00'),
(2013, 3, 'Rep.Dominicana', '80.20'),
(2013, 4, 'Argentina', '0.00'),
(2013, 4, 'Belice', '0.00'),
(2013, 4, 'Colombia', '0.00'),
(2013, 4, 'Costa Rica', '17.80'),
(2013, 4, 'El Salvador', '22.50'),
(2013, 4, 'Guatemala', '0.00'),
(2013, 4, 'Honduras', '56.80'),
(2013, 4, 'Nicaragua', '0.00'),
(2013, 4, 'Panama', '0.00'),
(2013, 4, 'Rep.Dominicana', '80.20'),
(2013, 5, 'Argentina', '0.00'),
(2013, 5, 'Belice', '0.00'),
(2013, 5, 'Colombia', '0.00'),
(2013, 5, 'Costa Rica', '17.80'),
(2013, 5, 'El Salvador', '22.50'),
(2013, 5, 'Guatemala', '0.00'),
(2013, 5, 'Honduras', '56.80'),
(2013, 5, 'Nicaragua', '3.00'),
(2013, 5, 'Panama', '0.00'),
(2013, 5, 'Rep.Dominicana', '80.20'),
(2013, 9, 'Costa Rica', '549.80'),
(2013, 9, 'El Salvador', '199.20'),
(2013, 9, 'Guatemala', '18.80'),
(2013, 9, 'Honduras', '159.80'),
(2013, 9, 'Nicaragua', '336.30'),
(2013, 9, 'Panama', '10.00'),
(2013, 9, 'Rep.Dominicana', '80.20');

-- --------------------------------------------------------

--
-- Table structure for table `dsm`
--
-- Creation: Apr 04, 2014 at 09:17 PM
--

DROP TABLE IF EXISTS `dsm`;
CREATE TABLE IF NOT EXISTS `dsm` (
  `ye` year(4) NOT NULL,
  `mes` int(11) NOT NULL,
  `key` varchar(25) COLLATE utf8_bin NOT NULL,
  `cant` decimal(11,2) NOT NULL,
  PRIMARY KEY (`ye`,`mes`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `dsm`
--

INSERT INTO `dsm` (`ye`, `mes`, `key`, `cant`) VALUES
(2010, 12, 'Argentina', '0.00'),
(2010, 12, 'Belice', '0.50'),
(2010, 12, 'Colombia', '0.00'),
(2010, 12, 'Costa Rica', '425.40'),
(2010, 12, 'El Salvador', '206.00'),
(2010, 12, 'Guatemala', '172.30'),
(2010, 12, 'Honduras', '229.80'),
(2010, 12, 'Nicaragua', '114.90'),
(2010, 12, 'Panama', '3.50'),
(2010, 12, 'Rep.Dominicana', '3.70'),
(2011, 10, 'Argentina', '0.00'),
(2011, 10, 'Belice', '0.00'),
(2011, 10, 'Colombia', '0.00'),
(2011, 10, 'Costa Rica', '391.30'),
(2011, 10, 'El Salvador', '182.80'),
(2011, 10, 'Guatemala', '374.60'),
(2011, 10, 'Honduras', '265.20'),
(2011, 10, 'Nicaragua', '96.40'),
(2011, 10, 'Panama', '9.10'),
(2011, 10, 'Rep.Dominicana', '52.30'),
(2012, 5, 'Costa Rica', '217.60'),
(2012, 5, 'El Salvador', '140.40'),
(2012, 5, 'Guatemala', '58.50'),
(2012, 5, 'Honduras', '143.10'),
(2012, 5, 'Nicaragua', '47.10'),
(2012, 6, 'Argentina', '0.00'),
(2012, 6, 'Belice', '0.00'),
(2012, 6, 'Colombia', '0.00'),
(2012, 6, 'Costa Rica', '230.00'),
(2012, 6, 'El Salvador', '197.90'),
(2012, 6, 'Guatemala', '67.00'),
(2012, 6, 'Honduras', '161.60'),
(2012, 6, 'Nicaragua', '60.60'),
(2012, 6, 'Panama', '0.00'),
(2012, 6, 'Rep.Dominicana', '86.00'),
(2012, 7, 'Costa Rica', '278.60'),
(2012, 7, 'El Salvador', '208.30'),
(2012, 7, 'Guatemala', '79.59'),
(2012, 7, 'Honduras', '182.80'),
(2012, 7, 'Nicaragua', '68.60'),
(2012, 8, 'Argentina', '0.00'),
(2012, 8, 'Belice', '0.00'),
(2012, 8, 'Colombia', '0.00'),
(2012, 8, 'Costa Rica', '323.40'),
(2012, 8, 'El Salvador', '235.00'),
(2012, 8, 'Guatemala', '92.50'),
(2012, 8, 'Honduras', '194.70'),
(2012, 8, 'Nicaragua', '71.80'),
(2012, 8, 'Panama', '1.60'),
(2012, 8, 'Rep.Dominicana', '97.70'),
(2012, 9, 'Argentina', '0.00'),
(2012, 9, 'Belice', '0.00'),
(2012, 9, 'Colombia', '0.00'),
(2012, 9, 'Costa Rica', '328.20'),
(2012, 9, 'El Salvador', '267.70'),
(2012, 9, 'Guatemala', '110.20'),
(2012, 9, 'Honduras', '221.00'),
(2012, 9, 'Nicaragua', '81.40'),
(2012, 9, 'Panama', '1.60'),
(2012, 9, 'Rep.Dominicana', '97.70'),
(2012, 10, 'Argentina', '0.00'),
(2012, 10, 'Belice', '0.00'),
(2012, 10, 'Colombia', '0.00'),
(2012, 10, 'Costa Rica', '366.00'),
(2012, 10, 'El Salvador', '290.80'),
(2012, 10, 'Guatemala', '121.90'),
(2012, 10, 'Honduras', '255.90'),
(2012, 10, 'Nicaragua', '98.60'),
(2012, 10, 'Panama', '1.60'),
(2012, 10, 'Rep.Dominicana', '97.70'),
(2012, 11, 'Argentina', '0.00'),
(2012, 11, 'Belice', '0.00'),
(2012, 11, 'Colombia', '0.00'),
(2012, 11, 'Costa Rica', '418.00'),
(2012, 11, 'El Salvador', '302.30'),
(2012, 11, 'Guatemala', '141.80'),
(2012, 11, 'Honduras', '265.50'),
(2012, 11, 'Nicaragua', '110.00'),
(2012, 11, 'Panama', '1.60'),
(2012, 11, 'Rep.Dominicana', '97.70'),
(2012, 12, 'Argentina', '0.00'),
(2012, 12, 'Belice', '0.00'),
(2012, 12, 'Colombia', '0.00'),
(2012, 12, 'Costa Rica', '1530.40'),
(2012, 12, 'El Salvador', '314.40'),
(2012, 12, 'Guatemala', '157.80'),
(2012, 12, 'Honduras', '282.20'),
(2012, 12, 'Nicaragua', '127.60'),
(2012, 12, 'Panama', '1.60'),
(2012, 12, 'Rep.Dominicana', '97.70'),
(2013, 1, 'Argentina', '0.00'),
(2013, 1, 'Belice', '0.00'),
(2013, 1, 'Colombia', '0.00'),
(2013, 1, 'Costa Rica', '35.40'),
(2013, 1, 'El Salvador', '27.30'),
(2013, 1, 'Guatemala', '0.70'),
(2013, 1, 'Honduras', '14.30'),
(2013, 1, 'Nicaragua', '16.20'),
(2013, 1, 'Panama', '1.60'),
(2013, 1, 'Rep.Dominicana', '0.00'),
(2013, 2, 'Argentina', '0.00'),
(2013, 2, 'Belice', '0.50'),
(2013, 2, 'Colombia', '0.00'),
(2013, 2, 'Costa Rica', '104.40'),
(2013, 2, 'El Salvador', '59.40'),
(2013, 2, 'Guatemala', '2.40'),
(2013, 2, 'Honduras', '43.70'),
(2013, 2, 'Nicaragua', '28.60'),
(2013, 2, 'Panama', '1.60'),
(2013, 2, 'Rep.Dominicana', '0.00'),
(2013, 3, 'Argentina', '0.00'),
(2013, 3, 'Belice', '0.00'),
(2013, 3, 'Colombia', '0.00'),
(2013, 3, 'Costa Rica', '147.80'),
(2013, 3, 'El Salvador', '117.10'),
(2013, 3, 'Guatemala', '10.60'),
(2013, 3, 'Honduras', '61.30'),
(2013, 3, 'Nicaragua', '38.30'),
(2013, 3, 'Panama', '1.60'),
(2013, 3, 'Rep.Dominicana', '0.00'),
(2013, 4, 'Argentina', '0.00'),
(2013, 4, 'Belice', '1.40'),
(2013, 4, 'Colombia', '0.00'),
(2013, 4, 'Costa Rica', '222.40'),
(2013, 4, 'El Salvador', '126.50'),
(2013, 4, 'Guatemala', '61.10'),
(2013, 4, 'Honduras', '70.20'),
(2013, 4, 'Nicaragua', '51.10'),
(2013, 4, 'Panama', '6.40'),
(2013, 4, 'Rep.Dominicana', '0.00'),
(2013, 5, 'Argentina', '0.00'),
(2013, 5, 'Belice', '1.40'),
(2013, 5, 'Colombia', '0.00'),
(2013, 5, 'Costa Rica', '227.60'),
(2013, 5, 'El Salvador', '136.80'),
(2013, 5, 'Guatemala', '63.00'),
(2013, 5, 'Honduras', '79.60'),
(2013, 5, 'Nicaragua', '62.00'),
(2013, 5, 'Panama', '6.40'),
(2013, 5, 'Rep.Dominicana', '0.00'),
(2013, 9, 'Belice', '1.90'),
(2013, 9, 'Costa Rica', '381.50'),
(2013, 9, 'El Salvador', '244.20'),
(2013, 9, 'Guatemala', '113.20'),
(2013, 9, 'Honduras', '172.00'),
(2013, 9, 'Nicaragua', '114.10'),
(2013, 9, 'Panama', '11.20'),
(2013, 9, 'Rep.Dominicana', '31.00');

-- --------------------------------------------------------

--
-- Table structure for table `permits`
--
-- Creation: Apr 03, 2014 at 09:13 PM
--

DROP TABLE IF EXISTS `permits`;
CREATE TABLE IF NOT EXISTS `permits` (
  `permit` int(11) NOT NULL,
  `apermits` int(11) NOT NULL,
  `Pnom` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`permit`,`apermits`),
  KEY `fk_permits_apermits1_idx` (`apermits`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- RELATIONS FOR TABLE `permits`:
--   `apermits`
--       `apermits` -> `apermits`
--

--
-- Dumping data for table `permits`
--

INSERT INTO `permits` (`permit`, `apermits`, `Pnom`) VALUES
(1, 1, 'Presidencia'),
(1, 2, 'Presidencia'),
(2, 1, 'Auditoria Interna');

-- --------------------------------------------------------

--
-- Table structure for table `sap_pa`
--
-- Creation: Apr 04, 2014 at 09:17 PM
--

DROP TABLE IF EXISTS `sap_pa`;
CREATE TABLE IF NOT EXISTS `sap_pa` (
  `ye` year(4) NOT NULL,
  `mes` int(11) NOT NULL,
  `key` varchar(25) COLLATE utf8_bin NOT NULL,
  `cant` decimal(11,2) NOT NULL,
  PRIMARY KEY (`ye`,`mes`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sap_pa`
--

INSERT INTO `sap_pa` (`ye`, `mes`, `key`, `cant`) VALUES
(2010, 12, 'Belice', '0.50'),
(2010, 12, 'Colombia', '0.00'),
(2010, 12, 'Costa Rica', '1511.80'),
(2010, 12, 'El Salvador', '867.20'),
(2010, 12, 'Guatemala', '1113.20'),
(2010, 12, 'Honduras', '794.30'),
(2010, 12, 'Nicaragua', '459.90'),
(2010, 12, 'Panama', '17.50'),
(2010, 12, 'Rep.Dominicana', '50.00'),
(2011, 12, 'Belice', '0.50'),
(2011, 12, 'Colombia', '0.00'),
(2011, 12, 'Costa Rica', '1523.10'),
(2011, 12, 'El Salvador', '840.80'),
(2011, 12, 'Guatemala', '1243.80'),
(2011, 12, 'Honduras', '914.70'),
(2011, 12, 'Nicaragua', '478.90'),
(2011, 12, 'Panama', '26.60'),
(2011, 12, 'Rep.Dominicana', '102.30'),
(2012, 5, 'Belice', '0.00'),
(2012, 5, 'Costa Rica', '1559.50'),
(2012, 5, 'El Salvador', '810.00'),
(2012, 5, 'Guatemala', '1255.20'),
(2012, 5, 'Honduras', '1004.50'),
(2012, 5, 'Nicaragua', '493.10'),
(2012, 5, 'Panama', '26.60'),
(2012, 5, 'Rep.Dominicana', '183.30'),
(2012, 6, 'Belice', '0.50'),
(2012, 6, 'Colombia', '0.00'),
(2012, 6, 'Costa Rica', '1537.90'),
(2012, 6, 'El Salvador', '856.10'),
(2012, 6, 'Guatemala', '1254.00'),
(2012, 6, 'Honduras', '1008.30'),
(2012, 6, 'Nicaragua', '501.50'),
(2012, 6, 'Panama', '26.60'),
(2012, 6, 'Rep.Dominicana', '183.30'),
(2012, 7, 'Belice', '0.00'),
(2012, 7, 'Costa Rica', '1532.50'),
(2012, 7, 'El Salvador', '836.60'),
(2012, 7, 'Guatemala', '1245.00'),
(2012, 7, 'Honduras', '1000.60'),
(2012, 7, 'Nicaragua', '505.50'),
(2012, 7, 'Panama', '28.20'),
(2012, 7, 'Rep.Dominicana', '200.00'),
(2012, 9, 'Belice', '0.50'),
(2012, 9, 'Colombia', '0.00'),
(2012, 9, 'Costa Rica', '1517.10'),
(2012, 9, 'El Salvador', '883.40'),
(2012, 9, 'Guatemala', '1233.40'),
(2012, 9, 'Honduras', '1011.80'),
(2012, 9, 'Nicaragua', '504.30'),
(2012, 9, 'Panama', '28.20'),
(2012, 9, 'Rep.Dominicana', '200.00'),
(2012, 10, 'Belice', '0.50'),
(2012, 10, 'Colombia', '0.00'),
(2012, 10, 'Costa Rica', '1512.30'),
(2012, 10, 'El Salvador', '878.70'),
(2012, 10, 'Guatemala', '1236.90'),
(2012, 10, 'Honduras', '1038.50'),
(2012, 10, 'Nicaragua', '516.60'),
(2012, 10, 'Panama', '28.20'),
(2012, 10, 'Rep.Dominicana', '200.00'),
(2012, 11, 'Belice', '0.50'),
(2012, 11, 'Colombia', '0.00'),
(2012, 11, 'Costa Rica', '1529.30'),
(2012, 11, 'El Salvador', '878.30'),
(2012, 11, 'Guatemala', '1249.10'),
(2012, 11, 'Honduras', '1038.10'),
(2012, 11, 'Nicaragua', '524.50'),
(2012, 11, 'Panama', '28.20'),
(2012, 11, 'Rep. Dominicana', '200.20'),
(2012, 12, 'Belice', '0.50'),
(2012, 12, 'Colombia', '0.00'),
(2012, 12, 'Costa Rica', '1530.40'),
(2012, 12, 'El Salvador', '865.50'),
(2012, 12, 'Guatemala', '1255.50'),
(2012, 12, 'Honduras', '1043.90'),
(2012, 12, 'Nicaragua', '536.80'),
(2012, 12, 'Panama', '28.20'),
(2013, 1, 'Belice', '0.50'),
(2013, 1, 'Colombia', '0.00'),
(2013, 1, 'Costa Rica', '1502.50'),
(2013, 1, 'El Salvador', '872.50'),
(2013, 1, 'Guatemala', '1235.80'),
(2013, 1, 'Honduras', '1048.30'),
(2013, 1, 'Nicaragua', '546.90'),
(2013, 1, 'Panama', '29.80'),
(2013, 1, 'Rep.Dominicana', '194.60'),
(2013, 2, 'Belice', '0.50'),
(2013, 2, 'Colombia', '0.00'),
(2013, 2, 'Costa Rica', '1489.70'),
(2013, 2, 'El Salvador', '872.50'),
(2013, 2, 'Guatemala', '1277.70'),
(2013, 2, 'Honduras', '1065.10'),
(2013, 2, 'Nicaragua', '556.50'),
(2013, 2, 'Panama', '29.80'),
(2013, 2, 'Rep.Dominicana', '194.60'),
(2013, 3, 'Belice', '0.00'),
(2013, 3, 'Colombia', '0.00'),
(2013, 3, 'Costa Rica', '1506.24'),
(2013, 3, 'El Salvador', '904.40'),
(2013, 3, 'Guatemala', '1188.30'),
(2013, 3, 'Honduras', '1076.00'),
(2013, 3, 'Nicaragua', '555.50'),
(2013, 3, 'Panama', '29.80'),
(2013, 3, 'Rep.Dominicana', '194.60'),
(2013, 4, 'Belice', '1.80'),
(2013, 4, 'Colombia', '0.00'),
(2013, 4, 'Costa Rica', '1488.10'),
(2013, 4, 'El Salvador', '895.90'),
(2013, 4, 'Guatemala', '1259.60'),
(2013, 4, 'Honduras', '1075.00'),
(2013, 4, 'Nicaragua', '561.80'),
(2013, 4, 'Panama', '34.60'),
(2013, 4, 'Rep.Dominicana', '194.60'),
(2013, 5, 'Belice', '1.80'),
(2013, 5, 'Colombia', '0.00'),
(2013, 5, 'Costa Rica', '1458.00'),
(2013, 5, 'El Salvador', '894.60'),
(2013, 5, 'Guatemala', '1243.40'),
(2013, 5, 'Honduras', '1067.50'),
(2013, 5, 'Nicaragua', '561.80'),
(2013, 5, 'Panama', '34.60'),
(2013, 5, 'Rep.Dominicana', '194.60'),
(2013, 9, 'Belice', '2.30'),
(2013, 9, 'Colombia', '0.00'),
(2013, 9, 'Costa Rica', '1116.10'),
(2013, 9, 'El Salvador', '917.00'),
(2013, 9, 'Guatemala', '1241.50'),
(2013, 9, 'Honduras', '1125.10'),
(2013, 9, 'Nicaragua', '588.60'),
(2013, 9, 'Panama', '39.40'),
(2013, 9, 'Rep.Dominicana', '220.10');

-- --------------------------------------------------------

--
-- Table structure for table `sap_si`
--
-- Creation: Apr 04, 2014 at 09:18 PM
--

DROP TABLE IF EXISTS `sap_si`;
CREATE TABLE IF NOT EXISTS `sap_si` (
  `ye` year(4) NOT NULL,
  `mes` int(11) NOT NULL,
  `key` varchar(20) COLLATE utf8_bin NOT NULL,
  `cant` decimal(11,2) NOT NULL,
  PRIMARY KEY (`ye`,`mes`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sap_si`
--

INSERT INTO `sap_si` (`ye`, `mes`, `key`, `cant`) VALUES
(2010, 12, 'Privado', '1393.70'),
(2010, 12, 'Publico', '3420.10'),
(2011, 12, 'Privado', '1299.90'),
(2011, 12, 'Publico', '3830.80'),
(2012, 5, 'Privado', '1335.40'),
(2012, 5, 'Publico', '4002.20'),
(2012, 6, 'Privado', '1319.50'),
(2012, 6, 'Publico', '4053.70'),
(2012, 7, 'Privado', '1295.90'),
(2012, 7, 'Publico', '4052.90'),
(2012, 9, 'Privado', '1254.70'),
(2012, 9, 'Publico', '4123.70'),
(2012, 10, 'Privado', '1290.70'),
(2012, 10, 'Publico', '4121.00'),
(2012, 11, 'Privado', '1306.40'),
(2012, 11, 'Publico', '4141.60'),
(2012, 12, 'Privado', '1289.90'),
(2012, 12, 'Publico', '4165.60'),
(2013, 1, 'Privado', '1258.50'),
(2013, 1, 'Publico', '4172.40'),
(2013, 2, 'Privado', '1246.80'),
(2013, 2, 'Publico', '4189.50'),
(2013, 3, 'Privado', '1257.60'),
(2013, 3, 'Publico', '4229.60'),
(2013, 4, 'Privado', '1257.60'),
(2013, 4, 'Publico', '4229.60'),
(2013, 5, 'Privado', '1219.50'),
(2013, 5, 'Publico', '4236.80'),
(2013, 11, 'Privado', '1306.40'),
(2013, 11, 'Publico', '4141.60');

-- --------------------------------------------------------

--
-- Table structure for table `usr`
--
-- Creation: Apr 07, 2014 at 05:05 PM
--

DROP TABLE IF EXISTS `usr`;
CREATE TABLE IF NOT EXISTS `usr` (
  `usr` varchar(50) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(255) COLLATE utf8_bin NOT NULL,
  `permit` int(255) NOT NULL,
  PRIMARY KEY (`usr`),
  KEY `fk_usr_apermits_idx` (`permit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- RELATIONS FOR TABLE `usr`:
--   `permit`
--       `permits` -> `permit`
--

--
-- Dumping data for table `usr`
--

INSERT INTO `usr` (`usr`, `pwd`, `permit`) VALUES
('a', '1f40fc92da241694750979ee6cf582f2d5d7d28e18335de05abc54d0560e0f5302860c652bf08d560252aa5e74210546f369fbbbce8c12cfc7957b2652fe9a75', 1),
('q', '2e96772232487fb3a058d58f2c310023e07e4017c94d56cc5fae4b54b44605f42a75b0b1f358991f8c6cbe9b68b64e5b2a09d0ad23fcac07ee9a9198a745e1d5', 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permits`
--
ALTER TABLE `permits`
  ADD CONSTRAINT `fk_permits_apermits1` FOREIGN KEY (`apermits`) REFERENCES `apermits` (`apermits`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `usr`
--
ALTER TABLE `usr`
  ADD CONSTRAINT `fk_usr_apermits` FOREIGN KEY (`permit`) REFERENCES `permits` (`permit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

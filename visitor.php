<?php
require_once'template.php';
$start = new template;
$start->meta();
$start->head();
$start->pagestart();
$start->pagehead1();
$start->backbtn();
$start->pagehead2();
$start->contstart();
?>
    <center><img src="images/524-logo-de-bcie.jpg"style="max-width:5%; height:auto;"></center>
    <center><h2>Bienvenido! Para ver un grafico, seleccionelo en la lista</h2></center>
    <div data-role="collapsible-set">
       <?php
        for ($i=date("Y"); $i>(date("Y")-5); $i--) 
        {
           echo "<div data-role=\"collapsible\">";
           if ($i==date("Y"))
           {
               echo "<h2>".$i." (A la Fecha)</h2>";
           }
           else
           {
            echo "<h2>".$i."</h2>";
           }
           echo "<ul data-role=\"listview\">";
           echo"<li><a href=\"graphdisplay.php?var=Saldo%20Activo-Productivo%20Por%20Sector&var2=sap_si&var3=".$i."\">Saldo Activo-Productivo Por Sector</a></li>";
           echo"<li><a href=\"graphdisplay.php?var=Saldo%20Activo-Productivo%20Por%20País&var2=sap_pa&var3=".$i."\">Saldo Activo-Productivo Por País</a></li>";
           echo"<li><a href=\"graphdisplay.php?var=Desembolsos%20".$i."&var2=dsm&var3=".$i."\">Desembolsos</a></li>";
           echo"<li><a href=\"graphdisplay.php?var=Aprobaciones%20".$i."&var2=aprv&var3=".$i."\">Aprobaciones</a></li>";
           echo "</div>";
        }
       ?>
    </div>
<?php
$start->endpage();
?>

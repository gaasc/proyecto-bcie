<?php
class graphs
{
        private $name;
        private $title;
        private $data;
    public function chartp($title,$data,$type)
    {
        echo "<script>";
        echo"$(document).on(\"pageshow\", function(){var chart = new CanvasJS.Chart(\"chartContainer\",";
        echo"{title:{text: \"".$title."\"},";
        echo"data: [";
        echo"{type: \"".$type."\",showInLegend: true,startAngle: -90,dataPoints:";
        echo"[";
         for ($i=0; $i <count($data) ; $i++) 
        { 
            echo "{label:\"".$data[$i][0]."\",legendText:\"".$data[$i][0].":".$data[$i][1]."\",y:".$data[$i][1].",indexLabel:\"".$data[$i][0]."\"}";
            if ($i<(count($data)-1)) 
            {
                echo ",";
            }
        }
        echo"]";
        echo"}";
        echo"]";
        echo"});chart.render();});";
        echo "</script>";
    }
    public function chartc($title,$data)
    {
        echo "<script>";
        echo"$(document).on(\"pageshow\", function(){var chart = new CanvasJS.Chart(\"chartContainer\",";
        echo"{title:{text: \"".$title."\"},";
        echo"data: [";
        echo"{type: \"column\",dataPoints:";
        echo"[";
         for ($i=0; $i <count($data) ; $i++) 
        { 
            echo "{label:\"".$data[$i][0]."\",legendText:\"".$data[$i][0].":".$data[$i][1]."\",y:".$data[$i][1]."}";
            if ($i<(count($data)-1)) 
            {
                echo ",";
            }
        }
        echo"]";
        echo"}";
        echo"]";
        echo"});chart.render();});";
        echo "</script>";
    }
}
?>
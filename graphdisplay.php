<?php
if(!$_GET)
{
    echo "<script type='text/javascript'>alert('Error: Ingreso no valido, por favor seleccione un grafico diferente'); window.location.href = \"Indice.php\";</script>";
}
require_once'template.php';
require_once'graphs.php';
require_once'bd.php';
$display = new graphs;
$start = new template;
$BD=new bd;
$start->meta();
$start->head();
$start->pagestart();
$data=$BD->inno($_GET);
switch($_GET["var2"])
{
    case 'sap_si':
    case 'sap_pa':
    $display->chartp($_GET["var"],$data,"pie");
    break;
    case'dsm':
    case'aprv':
    $display->chartc($_GET["var"],$data);
    break;
    default:
    $display->chartp($_GET["var"],$data,"pie");
}
?>
<script type="text/javascript" src="themes/canvasjs.min.js"></script>
<?php
$start->pagehead1();
$start->backbtn();
$start->pagehead2();
$start->contstart();
?>
<div id="chartContainer" style="height: 100vh; width: 100%;">
</div>
<?php
$start->endpage();
?>
